#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from osgeo import gdal
import math
import numpy as np
from scripts import create_filter


def get_origin(raster):
    """
    Parameters
    ----------
    raster_filename : str
        Raster path

    Returns
    -------
    x_origin : float
        x coordinate of top lef pixel
    y_origin : float
        y coordinate of top lef pixel
    """
    transform = raster.GetGeoTransform(can_return_null=True)
    x_origin, y_origin = transform[0], transform[3]

    return x_origin, y_origin


def pixel_coord_2_pos(raster, coord_x, coord_y, mode='int'):
    geotransform = raster.GetGeoTransform()
    origin_x = geotransform[0]
    origin_y = geotransform[3]
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]

    if mode == 'int':
        col = int((coord_x - origin_x) / pixel_width)
        row = int((coord_y - origin_y) / pixel_height)
    elif mode == 'absolute':
        col = (coord_x - origin_x) / pixel_width
        row = (coord_y - origin_y) / pixel_height
    #coordX = originX+pixelWidth*xOffset
    #coordY = originY+pixelHeight*yOffset
    return row, col


def pixel_pos_2_coord(raster, row, col):

    geotransform = raster.GetGeoTransform()
    origin_x = geotransform[0]
    origin_y = geotransform[3]
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]

    x = origin_x + col * pixel_width + pixel_width / 2
    y = origin_y + row * pixel_height + pixel_height / 2

    return x, y


def raster_adjustement(raster, dim_adjustement, value_extraction=None):
    gdal.AllRegister()

    transform = list(raster.GetGeoTransform())
    new_origin = pixel_pos_2_coord(raster, -dim_adjustement[0], -dim_adjustement[1])

    transform[0] = new_origin[0] - (transform[1]/2.)
    transform[3] = new_origin[1] - (transform[5]/2.)

    fill_x_right = (raster.RasterXSize+dim_adjustement[1])
    fill_y_bottom = (raster.RasterYSize+dim_adjustement[0])
    outdata_x_size = fill_x_right + 21
    outdata_y_size = fill_y_bottom + 21

    driver = gdal.GetDriverByName('Mem')
    outdata = driver.Create("", outdata_x_size, outdata_y_size,
                            raster.RasterCount, gdal.GDT_Float32)

    outdata.SetGeoTransform(transform)
    outdata.SetProjection(raster.GetProjection())
    outdata.GetRasterBand(1).SetNoDataValue(np.nan)
    data_raster = raster.GetRasterBand(1).ReadAsArray().astype(np.float32)

    adjust_Data = np.zeros((outdata.RasterYSize, outdata.RasterXSize))
    adjust_Data.fill(np.nan)

    if np.isnan(raster.GetRasterBand(1).GetNoDataValue()) :
        data_raster = np.where(~np.isnan(data_raster), 1, np.nan) 
         * np.where(data_raster == value_extraction if value_extraction != None else data_raster, 1, 0)
    else :
        data_raster = np.where(data_raster == raster.GetRasterBand(1).GetNoDataValue(), np.nan, 1)
         * np.where(data_raster == value_extraction if value_extraction != None else data_raster, 1, 0)
    
    if dim_adjustement[0] <= 0 and dim_adjustement[1] <= 0 :
        adjust_Data[:fill_y_bottom, :fill_x_right] = data_raster[-dim_adjustement[0]: , -dim_adjustement[1]:]
    elif dim_adjustement[0] <= 0 and dim_adjustement[1] > 0 :
        adjust_Data[:fill_y_bottom, dim_adjustement[1]:fill_x_right] = data_raster[-dim_adjustement[0]: , :]
    elif dim_adjustement[0] > 0 and dim_adjustement[1] <= 0 :
        adjust_Data[dim_adjustement[0]:fill_y_bottom, :fill_x_right] = data_raster[: , -dim_adjustement[1]:]
    elif dim_adjustement[0] > 0 and dim_adjustement[1] > 0 :
        adjust_Data[dim_adjustement[0]:fill_y_bottom, dim_adjustement[1]:fill_x_right] = data_raster[: , :]
    else :
        raise "How did you end up here ?"

    data_raster = None
    outdata.GetRasterBand(1).WriteArray(adjust_Data)
    outdata.FlushCache()

    return outdata


def create_output_raster(lsr_raster, dst="output.tif"):
    """
        Create an empty raster

        Parameters
        ----------
            lsr_raster : GDal Image
                Low Spatial Resolution raster
    """

    gdal.AllRegister()

    driver = gdal.GetDriverByName('GTiff')
    outdata = driver.Create(dst, lsr_raster.RasterXSize, lsr_raster.RasterYSize,
                            lsr_raster.RasterCount, gdal.GDT_Float32,
                            options=['COMPRESS=LZW'])

    outdata.SetGeoTransform(lsr_raster.GetGeoTransform())
    outdata.SetProjection(lsr_raster.GetProjection())
    outdata.GetRasterBand(1).SetNoDataValue(np.nan)

    return outdata


def raster_projection(raster, reference):
    warp_option = gdal.WarpOptions(format="Mem", srcNodata=raster.GetRasterBand(1).GetNoDataValue(),
                                   dstNodata="NaN", outputType=gdal.GDT_Float32,
                                   srcSRS=raster.GetProjection(), dstSRS=reference.GetProjection(),
                                   resampleAlg="near", multithread=True,
                                   xRes=raster.GetGeoTransform()[1],
                                   yRes=raster.GetGeoTransform()[5])
    return(gdal.Warp("", raster, options=warp_option))
