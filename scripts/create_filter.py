#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
# sys.path.append('/DATA/CARHAB/03_scripts/raster-intersection/scripts')

from scripts import tools
import scipy as sp
import math

global LSR_P_SIZE

HRS_P_SIZE = 1.5
#DX_MAX = HRS_P_SIZE * 3.5
DX_MAX = HRS_P_SIZE * 2.5
DY_MAX = DX_MAX

# TODO :
# Comment la variable TH_FILTER_SIZE est 
# affectée ? LSR_P_SIZE / 4 ?
# Pourquoi 0.25 ?
TH_FILTER_SIZE = 0.25  # it might be LSR_P_SIZE / 4

def find_first_lsr_pixel_rowcol(lsr_raster, hrs_raster):
    """
    Compare a Low Spatial Resolution (lsr) image and a
    High Spatial Resolution (hrs) image and find the first lsr pixel whose
    centroid is include in the hrs image.

    Parameters
    ----------
    lsr_raster : str
        Path of the lsr raster file
    hrs_raster : str
        Path of the hrs raster file

    Returns
    -------
    row_lsr : int
        row index of the lsr pixel
    col_lsr : int
        column index of the lsr pixel
    """

    global LSR_P_SIZE

    LSR_P_SIZE = lsr_raster.GetGeoTransform()[1]

    # coordinates of the centroid of top left pixel of the two image
    x_origin_lsr, y_origin_lsr = tools.get_origin(lsr_raster)
    x_origin_hrs, y_origin_hrs = tools.get_origin(hrs_raster)

    offset_x = x_origin_hrs - x_origin_lsr
    offset_y = y_origin_hrs - y_origin_lsr

    if offset_x <= 0:  # the lsr image is further east than the hrs image
        col_lsr = 0
    else:
        # number of "empty" lsr pixels
        col_lsr = offset_x // LSR_P_SIZE

        # add one or two pixels depending of the offset
        reste = offset_x % LSR_P_SIZE
        col_lsr = col_lsr + 1 if reste < (LSR_P_SIZE / 2) else col_lsr + 2

        # indexation starts at 0
        col_lsr -= 1

    if offset_y >= 0:  # the lsr image is further south than the hrs image
        row_lsr = 0
    else:
        offset_y = -offset_y

        # number of "empty" lsr pixels
        row_lsr = offset_y // LSR_P_SIZE

        # add one or two pixels depending of the offset
        reste = offset_y % LSR_P_SIZE
        row_lsr = row_lsr + 1 if reste < (LSR_P_SIZE / 2) else row_lsr + 2

        # indexation starts at 0
        row_lsr -= 1

    return int(row_lsr), int(col_lsr)


def find_offset(x_lsr, y_lsr, x_hrs, y_hrs):
    """
    Given the centroid of lsr pixel and the nearest hrs pixel centroid, the
    function return the distances of the part included of the hrs pixels that
    intersect the lsr pixel.

    Parameters
    ----------
    x_lsr : float
        x coordinates of the centroide of the lsr pixel
    y_lsr : float
        y coordinates of the centroide of the lsr pixel
    x_hrs : float
        x coordinates of the centroide of the hrs pixel
    y_hrs: float
        y coordinates of the centroide of the hrs pixel

    Returns
    -------
    dx_left : float
        Distance between left boundary of the hrs pixel and the right boundary
        of the lsr image. dx_left < 1.5
    dx_right : float
        Distance between right boundary of the hrs pixel and the left boundary
        of the lsr image. dx_right < 1.5
    dy_top : float
        Distance between bottom boundary of the hrs pixel and the top boundary
        of the lsr image. dy_top < 1.5
    dy_bottom : float
        Distance between top boundary of the hrs pixel and the bottom boundary
        of the lsr image. dy_bottom < 1.5

    """

    # can be factorized
    offset_x = x_lsr - x_hrs
    dx_x_hrs_to_bound = offset_x + LSR_P_SIZE / 2
    #dx_right = (HRS_P_SIZE + dx_x_hrs_to_bound - DX_MAX) % HRS_P_SIZE
    dx_right = (dx_x_hrs_to_bound - DX_MAX) % HRS_P_SIZE
    dx_x_hrs_to_bound = LSR_P_SIZE / 2 - offset_x
    dx_left = (dx_x_hrs_to_bound - DX_MAX) % HRS_P_SIZE
    #dx_left = ((2.5 - dx_right) % HRS_P_SIZE)

    offset_y = y_lsr - y_hrs
    dy_y_hrs_to_bound = offset_y + LSR_P_SIZE / 2
    #dy_top = (HRS_P_SIZE + dy_y_hrs_to_bound - DY_MAX) % HRS_P_SIZE
    dy_top = (dy_y_hrs_to_bound - DY_MAX) % HRS_P_SIZE
    dy_y_hrs_to_bound = LSR_P_SIZE / 2 - offset_y
    dy_bottom = (dy_y_hrs_to_bound - DY_MAX) % HRS_P_SIZE
    #dy_bottom = ((2.5 - dy_top) % HRS_P_SIZE)

    return dx_left, dx_right, dy_top, dy_bottom


def get_dim_to_fill(lsr_raster, hrs_raster):
    """
    The cube_filter meant to be applied on the top left pixel of an image, but
    due to offset between VHSR and LSR image, it may be some rows and columns
    to add to the hrs image.

    So the function returns the numbers of columns and rows to be added to the
    THRS image before applying the cube_filter.

    `n_row_fill` and `n_col_fill` can be negative and means that `n_row_fill`
    or`n_col_fill` need to be remove from the THRS image

    Parameters
    ----------
    lsr_filename : str
        Path of the lsr raster file
    hrs_filename : str
        Path of the hrs raster file

    Returns
    -------
    nrow_fill : int
        Number of rows to fill so that the filter can be applied
    ncol_fill : int
        Number of columns to fill so that the filter can be applied

    Limitations
    -----------
    Unknwow behaviour in case the two images are aligned
    """
    # get the coordinate of lsr centroid and nearest hrs's pixel neighbor
    # centroid
    row_lsr, col_lsr = find_first_lsr_pixel_rowcol(lsr_raster, hrs_raster)
    x_lsr, y_lsr = tools.pixel_pos_2_coord(lsr_raster, row_lsr, col_lsr)

    r_hrs, c_hrs = tools.pixel_coord_2_pos(hrs_raster, x_lsr, y_lsr,
                                           mode='int')
    x_hrs, y_hrs = tools.pixel_pos_2_coord(hrs_raster, r_hrs, c_hrs)

    # real dy dist : pixel centroid to boundary
    dx_real = c_hrs * HRS_P_SIZE + HRS_P_SIZE / 2

    # theoretical dy dist : pixel centroid to boundary
    offset_x = x_lsr - x_hrs
    dx_theori = LSR_P_SIZE / 2 - offset_x

    # convert dx offset to the number of missing col
    offset_dx = dx_theori - dx_real
    if offset_dx > 0:
        ncol_fill = offset_dx // HRS_P_SIZE
        ncol_fill += 1 if offset_dx % HRS_P_SIZE > 0 else ncol_fill
    else:
        ncol_fill = abs(offset_dx) // HRS_P_SIZE
        ncol_fill = - ncol_fill

    # real dy dist : pixel centroid to boundary
    dy_real = r_hrs * HRS_P_SIZE + HRS_P_SIZE / 2

    # theoretical dy dist : pixel centroid to boundary
    offset_y = y_lsr - y_hrs
    dy_theori = LSR_P_SIZE / 2 + offset_y

    # convert dx offset to the number of missing col
    offset_dy = dy_theori - dy_real
    if offset_dy > 0:
        nrow_fill = offset_dy // HRS_P_SIZE
        nrow_fill += 1 if offset_dy % HRS_P_SIZE > 0 else nrow_fill
    else:
        nrow_fill = abs(offset_dy) // HRS_P_SIZE
        nrow_fill = - nrow_fill

    return int(nrow_fill), int(ncol_fill)


def get_filter_size(x_lsr, y_lsr, x_hrs, y_hrs):
    """
    Given the centroid of lsr pixel and the nearest hrs pixel centroid, the
    function return the size of the corresponding filter

    Parameters
    ----------
    x_lsr : float
        x coordinates of the centroide of the lsr pixel
    y_lsr : float
        y coordinates of the centroide of the lsr pixel
    x_hrs : float
        x coordinates of the centroide of the hrs pixel
    y_hrs: float
        y coordinates of the centroide of the hrs pixel

    Returns
    -------
    n_row : int
        Number of row of the filter
    n_col : int
        Number of columns of the filter

    """

    offset_x = x_lsr - x_hrs
    dist_to_bound = LSR_P_SIZE / 2 + offset_x - HRS_P_SIZE / 2
    n_pix_right = int(math.ceil(dist_to_bound / HRS_P_SIZE))
    dist_to_bound = LSR_P_SIZE / 2 - offset_x - HRS_P_SIZE / 2
    n_pix_left = int(math.ceil(dist_to_bound / HRS_P_SIZE))
    n_col = n_pix_right + n_pix_left + 1

    offset_y = y_lsr - y_hrs
    dist_to_bound = LSR_P_SIZE / 2 + offset_y - HRS_P_SIZE / 2
    n_pix_top = int(math.ceil(dist_to_bound / HRS_P_SIZE))
    dist_to_bound = LSR_P_SIZE / 2 - offset_y - HRS_P_SIZE / 2
    n_pix_bot = int(math.ceil(dist_to_bound / HRS_P_SIZE))
    n_row = n_pix_top + n_pix_bot + 1

    #n_col = 8 if abs(offset_x) > TH_FILTER_SIZE else 7
    #n_row = 8 if abs(offset_y) > TH_FILTER_SIZE else 7

    return n_row, n_col


def create_sub_filter(dx_left, dx_right, dy_top, dy_bottom, n_row, n_col):
    """
    Given the distances of the part included of the hrs pixels that
    intersect the lsr pixel, and the size of the filter, the function returns
    an 2d array containing the weights corresponding of the proportion of the
    hrs pixels that are included in a lsr pixel

    Parameters
    ----------
    dx_left : float
        Distance between left boundary of the hrs pixel and the right boundary
        of the lsr image. dx_left < 1.5
    dx_right : float
        Distance between right boundary of the hrs pixel and the left boundary
        of the lsr image. dx_right < 1.5
    dy_top : float
        Distance between bottom boundary of the hrs pixel and the top boundary
        of the lsr image. dy_top < 1.5
    dy_bottom : float
        Distance between top boundary of the hrs pixel and the bottom boundary
        of the lsr image. dy_bottom < 1.5
    n_row : int
        Number of row of the filter
    n_col : int
        Number of columns of the filter

    Returns
    -------
    sub_filter : 2d array
        Contains the weights ([0 ; 1]) corresponding of the proportion of the
        hrs pixels that are included in a lsr pixel.
    """

    # Compute weights given the offsets
    weight_left = (dx_left * HRS_P_SIZE) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_right = (dx_right * HRS_P_SIZE) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_top = (dy_top * HRS_P_SIZE) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_bottom = (dy_bottom * HRS_P_SIZE) / (HRS_P_SIZE * HRS_P_SIZE)
    # the corners
    weight_topleft = (dy_top * dx_left) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_topright = (dy_top * dx_right) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_bottomleft = (dy_bottom * dx_left) / (HRS_P_SIZE * HRS_P_SIZE)
    weight_bottomright = (dy_bottom * dx_right) / (HRS_P_SIZE * HRS_P_SIZE)

    # fill the sub_filter
    sub_filter = sp.ones((n_row, n_col))
    sub_filter[:, 0] = weight_left
    sub_filter[:, -1] = weight_right
    sub_filter[0, :] = weight_top
    sub_filter[-1, :] = weight_bottom
    sub_filter[0, 0] = weight_topleft
    sub_filter[0, -1] = weight_topright
    sub_filter[-1, 0] = weight_bottomleft
    sub_filter[-1, -1] = weight_bottomright

    return sub_filter


def create_one_band_filter(r_init, c_init, x_lsr, y_lsr, x_hrs, y_hrs):
    """
    The function create a band of the cube_filter filled with a sub_filter

    Parameters
    ----------
    r_init : int
        top left row index where the sub_filter take place in the filter
    c_init : int
        top left column index where the sub_filter take place in the filter
    x_lsr : float
        x coordinates of the centroide of the lsr pixel
    y_lsr : float
        y coordinates of the centroide of the lsr pixel
    x_hrs : float
        x coordinates of the centroide of the hrs pixel
    y_hrs: float
        y coordinates of the centroide of the hrs pixel

    Returns
    -------
    one_band_filter : 2d array

    """

    # get offset
    dx_left, dx_right, dy_top, dy_bottom = find_offset(x_lsr, y_lsr,
                                                       x_hrs, y_hrs)

    # get the corresponding subfilter
    n_row, n_col = get_filter_size(x_lsr, y_lsr, x_hrs, y_hrs)
    sub_filter = create_sub_filter(dx_left, dx_right, dy_top, dy_bottom,
                                   n_row, n_col)

    # fill the band with the subfilter at the right location
    one_band_filter = sp.zeros((21, 21))
    r_end = r_init + n_row
    c_end = c_init + n_col

    one_band_filter[r_init: r_end, c_init: c_end] = sub_filter

    return one_band_filter


def create_cube_filter(lsr_raster, hrs_raster):
    """
    Create a cube filter of the proportion of VHSR pixels included in a
    neighbourhood of nxn pixels of the LSR image.

    For example if the size of neighbourhood is 3x3 pixels of lsr image of a
    pixel size of 10m, it corresponds to 21*21 pixels of hrs 6-7 image of
    pixel size of 1.5m. The proportion of hrs pixels included in a cell
    (=a pixel lsr) is different from the other cells. So the function compute a
    sub filter for each cell and store it in a neighbourhood matrix, the other
    cell having a proportion set to 0.

    So, each band (third dimension) of the cube_filter correspond to the filter
    of one pixel of the LSR image (lsr in this example).

    Here an example of the 3x3 lsr neighbourhood, with different weights w for
    each band.

            Band 1                   Band 2
     ______ ______ ______    ______ ______ ______
    |      |      |      |  |      |      |      |
    |  w1  |   0  |  0   |  |  0   |  w2  |   0  |
    |______|______|______|  |______|______|______|
    |      |      |      |  |      |      |      |
    |  0   |   0  |  0   |  |  0   |  0   |   0  |    ... ... ... ...
    |______|______|______|  |______|______|______|
    |      |      |      |  |      |      |      |
    |  0   |   0  |  0   |  |  0   |  0   |   0  |
    |______|______|______|  |______|______|______|


    Note that the size each sub filter may be different, and the one VHSR pixel
    can be considered for multiple filter (max 4).


    Parameters
    ----------
    lsr_filename : str
        Path of the lsr raster file
    hrs_filename : str
        Path of the hrs raster file

    Returns
    -------
    cube_filter : 3d array

    """

    # get coordinates and index of the lsr pixel
    row_lsr, col_lsr = find_first_lsr_pixel_rowcol(lsr_raster, hrs_raster)
    x_lsr, y_lsr = tools.pixel_pos_2_coord(lsr_raster, row_lsr, col_lsr)

    # get coordinates and index of the nearest hrs pixel
    r_hrs, c_hrs = tools.pixel_coord_2_pos(hrs_raster, x_lsr, y_lsr,
                                           mode='int')
    x_hrs, y_hrs = tools.pixel_pos_2_coord(hrs_raster, r_hrs, c_hrs)

    # initialize the cube
    cube_filter = sp.zeros((21, 21, 9))
    r_init = 0
    c_init = 0
    # fill with the first band
    cube_filter[:, :, 0] = create_one_band_filter(r_init, c_init, x_lsr, y_lsr,
                                                  x_hrs, y_hrs)

    # iterate over the remaining sub_filter/band
    i = 0
    y_lsr_offset = y_lsr
    for _ in range(3):   # iter on rows
        c_init = 0
        x_lsr_offset = x_lsr
        for _ in range(3):  # iter on columns
            if i == 0:  # already filled, just increment the offsets
                n_row, n_col = get_filter_size(x_lsr_offset, y_lsr_offset,
                                               x_hrs, y_hrs)
                c_init += n_col - 1
                x_lsr_offset += LSR_P_SIZE
                i += 1
                continue

            # get new nearest hrs pixel
            r_hrs, c_hrs = tools.pixel_coord_2_pos(hrs_raster, x_lsr_offset,
                                                   y_lsr_offset, mode='int')
            x_hrs, y_hrs = tools.pixel_pos_2_coord(hrs_raster, r_hrs,
                                                   c_hrs)
            n_row, n_col = get_filter_size(x_lsr_offset, y_lsr_offset,
                                           x_hrs, y_hrs)
            # fill with the sub_filter/band
            cube_filter[:, :, i] = create_one_band_filter(r_init, c_init,
                                                          x_lsr_offset,
                                                          y_lsr_offset,
                                                          x_hrs, y_hrs)

            c_init += n_col - 1
            x_lsr_offset += LSR_P_SIZE
            i += 1

        r_init += n_row - 1
        y_lsr_offset -= LSR_P_SIZE

    return cube_filter
