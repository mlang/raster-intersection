#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from skimage.util.shape import view_as_windows

import multiprocessing
import itertools

from scripts import create_filter
from scripts import tools


def read_nomenclature_file(nomenclature_file):
    try:
        with open(nomenclature_file, "r") as file:
            for line in file.readlines():
                yield(line.rstrip("\n").replace(" ", "_").split(":"))
    except Exception as e:
        print("Unable to open nomenclature file : {}.".format(nomenclature_file))


def convolution(window, filters):
    roi = []

    nan_matrix = ~np.isnan(window)
    for _filter in filters:
        if np.count_nonzero(nan_matrix*_filter) != np.count_nonzero(_filter):
            roi.append(np.nan)
        else:
            roi.append(np.nansum(window*_filter)/np.sum(_filter))

    return np.reshape(roi, (3, 3))


def apply_filters(view, filters, output_raster):

    output = output_raster.GetRasterBand(1).ReadAsArray()

    for x in range(view.shape[0]):
        for y in range(view.shape[1]):
            output[x*3:x*3+3, y*3:y*3+3] = convolution(view[x][y], filters)

    output_raster.GetRasterBand(1).WriteArray(output)
    output_raster.FlushCache()
    output_raster = None


def apply_filters_multiprocess(view, filters, row_col_start, output_raster):

    output = output_raster.GetRasterBand(1).ReadAsArray()

    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count())

    args = [(view[x][y], filters) for x, y in list(
        itertools.product(range(view.shape[0]), range(view.shape[1])))]
    results = pool.starmap(convolution, args)

    for i_tuple, result in zip([(x, y) for x, y in list(itertools.product(range(view.shape[0]), range(view.shape[1])))], results):
        x, y = i_tuple
        output[row_col_start[0]+(x*3):row_col_start[0]+(x*3+3),
               row_col_start[1]+(y*3):row_col_start[1]+(y*3+3)] = result

    output_raster.GetRasterBand(1).WriteArray(output)
    output_raster.FlushCache()
    output_raster = None
    results = None
    pool.close()
    pool.join()


def execute_and_write_output(lsr_raster, hrs_raster, cube_filter, dst, nomenclature=None, multiprocessing=False):

    nomenclature_list = read_nomenclature_file(
        nomenclature) if nomenclature != None else nomenclature

    filters = np.transpose(cube_filter, (2, 0, 1))
    dim_fill = create_filter.get_dim_to_fill(lsr_raster, hrs_raster)

    row_col_output_start = create_filter.find_first_lsr_pixel_rowcol(
        lsr_raster, hrs_raster)

    if nomenclature_list is None:
        useful_raster = tools.raster_adjustement(hrs_raster, dim_fill)
        view = view_as_windows(useful_raster.GetRasterBand(
            1).ReadAsArray(), (21, 21), (20, 20))
        output_raster = tools.create_output_raster(lsr_raster, dst=dst)
        if multiprocessing:
            apply_filters_multiprocess(view, filters, output_raster)
        else:
            apply_filters(view, filters, output_raster)
    else:
        for nomenclature_class in nomenclature_list:
            output_name = "{}_{}.tif".format(dst[:-4], nomenclature_class[0])
            useful_raster = tools.raster_adjustement(
                hrs_raster, dim_fill, value_extraction=int(nomenclature_class[1]))
            view = view_as_windows(useful_raster.GetRasterBand(
                1).ReadAsArray(), (21, 21), (20, 20))
            output_raster = tools.create_output_raster(
                lsr_raster, dst=output_name)
            if multiprocessing:
                apply_filters_multiprocess(
                    view, filters, row_col_output_start, output_raster)
            else:
                apply_filters(view, filters, output_raster)
