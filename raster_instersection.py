#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import argparse
import warnings

from osgeo import gdal

from scripts import create_filter
from scripts import apply_filter
from scripts import tools


def main(argv=sys.argv):

    start = time.time()

    try:
        hrs_raster = gdal.Open(argv.hrs)
        lrs_raster = gdal.Open(argv.lrs)
    except Exception as e:
        raise(e)

    hrs_reprojected = tools.raster_projection(hrs_raster, lrs_raster)

    cube_filter = create_filter.create_cube_filter(lrs_raster, hrs_reprojected)

    apply_filter.execute_and_write_output(
        lrs_raster, hrs_reprojected, cube_filter, argv.dst, argv.nomen, argv.mp)

    print("Execution in {} seconde(s)".format(time.time() - start))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-hrs", dest="hrs",
                        help="High resolution spatial raster.", required=True)
    parser.add_argument("-lrs", dest="lrs",
                        help="Low resolution spatial raster.", required=True)
    parser.add_argument("-dst", dest="dst",
                        help="Output file path.", required=True)
    parser.add_argument("-nomenclature", dest="nomen",
                        help="Nomenclature file.", required=False)
    parser.add_argument("-multiprocess", dest="mp", default=False,
                        action='store_true', help="Use multiprocessing.", required=False)

    args = parser.parse_args()

    with warnings.catch_warnings():
        if args.mp:
            warnings.simplefilter("once")
            warnings.warn(
                "Using multiprocessing consumes more resources and can lead to program failure.", ResourceWarning)

    sys.exit(main(args))
